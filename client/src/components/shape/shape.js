import { Typography, Card, CardActionArea, CardContent, CardMedia, Box, Grid, Button } from "@material-ui/core";
import React, { useState } from "react"

import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";

import shapeBoy1 from "../image/shape1.png";
import shapeBoy2 from "../image/shape2.png";
import shapeBoy3 from "../image/shape3.png";
import shapeBoy4 from "../image/shape4.png";
import shapeBoy5 from "../image/shape5.png";


import shapeGirl1 from "../image/shapeG1.png";
import shapeGirl2 from "../image/shapeG2.png";
import shapeGirl3 from "../image/shapeG3.png";
import shapeGirl4 from "../image/shapeG4.png";
import shapeGirl5 from "../image/shapeG5.png";
import shapeGirl6 from "../image/shapeG6.png";

const ShapeModel = {
    boys: [
        { shape: "Triangle", img: shapeBoy1 },
        { shape: "Rectangulaire", img: shapeBoy2 },
        { shape: "Trapèze", img: shapeBoy3 },
        { shape: "Triangle inversés", img: shapeBoy4 },
        { shape: "Ovale", img: shapeBoy5 },
    ],
    girls: [
        { shape: "Triangle", img: shapeGirl1 },
        { shape: "Rectangulaire", img: shapeGirl2 },
        { shape: "Sablier", img: shapeGirl3 },
        { shape: "Triangle inversés", img: shapeGirl4 },
        { shape: "Ovale", img: shapeGirl5 },
        { shape: "huit", img: shapeGirl6 },
    ]
}


const Shape = () => {
    const cookies = new Cookies();
    const [info] = useState(cookies.get('userInfo'))
    const [selected, setSelected] = useState(0);

    const navigate = useNavigate();
    var data = [{}]
    const [message, setmessage] = useState("");
    const handleClick = () => {
        if (selected === 0) {
            setmessage("veuillez renseigner votre silouette")
        }
        else {
            info.shape = selected
            cookies.set('userInfo', JSON.stringify({
                gender: info.gender,
                shape: selected
            }
            ), { path: '/' });
            navigate("/size")
        }
    }
    switch (info.gender) {
        case 1:
            data = ShapeModel.girls
            break;
        case 2:
            data = ShapeModel.boys
            break;
        case 3:
            data = ShapeModel.boys.concat(ShapeModel.girls)
            break;

        default:
            break;
    }

    console.log(ShapeModel)
    return (
        <>
            <Menu></Menu>
            <Box style={{ width: "99 %" }}><Typography className={"genderTitle"} style={{ marginBottom: "25px" }} >Choisissez votre silouette</Typography></Box>
            <Box style={{ width: "96%" }}>
                <Grid container justifyContent="center" spacing={6}>
                    {data.map((element, index) => {
                        return (
                            <Grid item>
                                <Card sx={{ maxWidth: 345, height: 450 }} style={{
                                    border: selected === element.shape ? "1px solid #F0C956" : "1px white solid"
                                }} className={"genderCard"} onClick={(e) => { setSelected(element.shape) }}>
                                    <CardActionArea>
                                        <CardMedia
                                            component="img"
                                            height="140"
                                            image={element.img}
                                            alt="green iguana"

                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {element.shape}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        )
                    })}
                </Grid>
            </Box>
            <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>
            <Grid container justifyContent="space-between">
                <Button className={'buttonPrev'} onClick={() => navigate("/gender")}>Précedent</Button>
                <Button className={'buttonGender'} onClick={() => handleClick()}>Suivant</Button>
            </Grid>

        </>

    )

}

export default Shape