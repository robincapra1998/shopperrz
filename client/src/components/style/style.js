import { Typography, Card, CardActionArea, CardContent, CardMedia, Box, Grid, Button } from "@material-ui/core";
import React, { useState } from "react"

import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";

import styleBoy1 from "../image/style1.png";
import styleBoy2 from "../image/style2.png";
import styleBoy3 from "../image/style3.png";
import styleBoy4 from "../image/style4.png";
import styleBoy5 from "../image/style5.png";
import styleBoy6 from "../image/style6.png";


import styleGirl1 from "../image/styleG1.png";
import styleGirl2 from "../image/styleG2.png";
import styleGirl3 from "../image/styleG3.png";
import styleGirl4 from "../image/styleG4.png";
import styleGirl5 from "../image/styleG5.png";
import styleGirl6 from "../image/styleG6.png";

const StyleModel = {
    boys: [
        { style: "Casual", img: styleBoy1 },
        { style: "Classic", img: styleBoy2 },
        { style: "Bohème", img: styleBoy3 },
        { style: "Minimaliste", img: styleBoy4 },
        { style: "Urban", img: styleBoy5 },
        { style: "Soirée", img: styleBoy6 },
    ],
    girls: [
        { style: "Casual", img: styleGirl1 },
        { style: "Classic", img: styleGirl2 },
        { style: "Bohème", img: styleGirl3 },
        { style: "Minimaliste", img: styleGirl4 },
        { style: "Urban", img: styleGirl5 },
        { style: "Soirée", img: styleGirl6 },
    ]
}


const Style = () => {
    const cookies = new Cookies();
    const [info] = useState(cookies.get('userInfo'))
    const [selected, setSelected] = useState(0);

    const navigate = useNavigate();
    var data = [{}]
    const [message, setmessage] = useState("");
    const handleClick = () => {
        if (selected === 0) {
            setmessage("veuillez renseigner votre silouette")
        }
        else {
            info.style = selected
            cookies.set('userInfo', JSON.stringify(info), { path: '/' });
            navigate("/need")
        }
    }
    switch (info.gender) {
        case 1:
            data = StyleModel.girls
            break;
        case 2:
            data = StyleModel.boys
            break;
        case 3:
            data = StyleModel.boys.concat(StyleModel.girls)
            break;

        default:
            break;
    }
    return (
        <>
            <Menu></Menu>
            <Box>
                <Typography className={"genderTitle"} style={{ marginBottom: "25px" }} >Quel est votre style ?</Typography>
            </Box>
            <Box style={{ width: "96%" }}>
                <Grid container justifyContent="center" spacing={6}>
                    {data.map((element, index) => {
                        return (
                            <Grid item>
                                <Card sx={{ maxWidth: 345, height: 450 }} style={{
                                    border: selected === element.style ? "1px solid #F0C956" : "1px white solid"
                                }} className={"genderCard"} onClick={(e) => { setSelected(element.style) }}>
                                    <CardActionArea>
                                        <CardMedia
                                            component="img"
                                            height="140"
                                            image={element.img}
                                            alt="green iguana"

                                        />
                                        <CardContent style={{ textAlign: "center" }}>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {element.style}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        )
                    })}
                </Grid>
            </Box>
            <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>
            <Grid container justifyContent="space-between">
                <Button className={'buttonPrev'} onClick={() => navigate("/size")}>Précedent</Button>
                <Button className={'buttonGender'} onClick={() => handleClick()}>Suivant</Button>
            </Grid>

        </>

    )

}

export default Style