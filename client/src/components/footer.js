import { Grid, makeStyles, Link, Button } from "@material-ui/core"
import React from 'react';
import { useNavigate } from "react-router-dom";
import Logo from './image/logo.png'


const useStyles = makeStyles(theme => ({
    menuList: {
        fontFamily: "satoshi",
        color: "white!important",
        textDecoration: "none",
        paddingRight: "100px",
        "&  a": {
            color: "white",
        }
    },
    container: {
        borderTop: "2px grey solid",
        marginBottom: "25px",
    },
    order: {
        backgroundColor: "#F0C956",
        fontWeight: 600,
        borderRadius: "25px",
        "&:hover": {
            border: "1px solid #F0C956",
            color: "white"
        }
    }

}));

export default function Footer() {
    const classes = useStyles()
    const navigate = useNavigate()
    return (
        <>
            <Grid
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
                className={classes.container}>


                <Grid item style={{ color: "white" }}>
                    <a href="/"> <img alt="shpz" className={"menulogo"} src={Logo}></img></a>
                </Grid>
                <Grid item className={classes.menuList}>
                    <Link className="menuLink" href={'/profile'}>Mon compte</Link>
                    <Link className="menuLink" href={'/'}>Nos offres</Link>
                    <Link className="menuLink">À propos</Link>
                    <Link className="menuLink">Contact</Link>

                    <Button onClick={() => {
                        navigate('/agenda')
                    }}>Reserver une séance</Button>
                </Grid>
            </Grid>

        </>
    )
}