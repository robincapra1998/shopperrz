import { Typography, Grid, } from "@material-ui/core";
import React, { useState } from "react"
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";
import config from "../../constant"


import { InlineWidget, useCalendlyEventListener } from "react-calendly";
import { useMutation } from "react-query";
const createOrderWithSchedul = async (data) => {
    console.log(data)
    if (!data) return null
    return fetch(`${config.url.API_URL}/schedule`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            event: data.payload.event,
            invitee: data.payload.invitee,
            token: data.token
        })
    }).then(res => res.json())

};

const More = () => {
    const cookies = new Cookies();


    //const navigate = useNavigate();

    const mutationOrderWithSchedul = useMutation(createOrderWithSchedul, {
        onSuccess: (data) => {
            console.log(data)
            if (data.status === 500) {

            }
            else {

            }
        },

    })

    useCalendlyEventListener({
        onProfilePageViewed: (e) => console.log(e),
        onDateAndTimeSelected: (e) => console.log(e),
        onEventTypeViewed: (e) => console.log(e),
        onEventScheduled: (e) => {
            mutationOrderWithSchedul.mutate({
                token: cookies.get("TOKEN"),
                ...e.data
            })
        },
    });

    const [message] = useState("");
    return (
        <>
            <Menu></Menu>
            <Grid container justifyContent="center" alignContent="center" direction="column" style={{
                margin: '0 auto',
                width: "40%",
            }}>
                <Grid item style={{ marginTop: "30px!important" }}>
                    <Typography className={"moreTitle"} style={{ marginBottom: "30px", textAlign: "center", marginTop: "30px!important" }}>Réservez votre Personnal Shopperzz</Typography>
                </Grid>
                <Grid item>
                    <Typography style={{ marginBottom: "30px", textAlign: 'center' }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                    </Typography>
                </Grid>
            </Grid>
            <InlineWidget url="https://calendly.com/robincapra/30min?hide_gdpr_banner=1" />
            <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>
        </>

    )

}

export default More