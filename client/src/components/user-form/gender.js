import { Typography, Card, CardActionArea, CardContent, CardMedia, Box, Grid, Button } from '@material-ui/core'
import React, { useState } from 'react'
import Menu from '../Menu'
import { useNavigate } from 'react-router-dom';

import "../../style/dashboard.css";

import femme from "../image/Rectangle37.png";
import homme from "../image/Rectangle34.png";
import ng from "../image/Rectangle35.png";

import Cookies from 'universal-cookie';


export default function GenderType() {
    const [selected, setSelected] = useState(0);

    const navigate = useNavigate();
    const [message, setmessage] = useState("");
    const cookies = new Cookies();
    const handleClick = () => {
        if (selected === 0) {
            setmessage("veuillez renseigner votre genre")
        }
        else {
            cookies.set('userInfo', JSON.stringify({
                gender: selected
            }
            ), { path: '/' });
            navigate("/shape")
        }


    }
    return (
        <>
            <Menu></Menu>
            <Box style={{ width: "99%" }}>
                <Grid container direction='column' >
                    <Grid item >
                        <Typography className={"genderTitle"} style={{ marginBottom: "25px" }}>Choisissez votre genre</Typography>
                    </Grid>

                    <Grid container justifyContent='center' spacing={3}>
                        <Grid item lg={3}>
                            <Card sx={{ maxWidth: 345, height: 450 }} style={{
                                border: selected === 1 ? "1px solid #F0C956" : "1px white solid"
                            }} className={"genderCard"} onClick={(e) => { setSelected(1) }}>
                                <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        height="140"
                                        image={femme}
                                        alt="green iguana"

                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                            Femme
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                        <Grid item lg={3}>
                            <Card sx={{ maxWidth: 345 }} style={{
                                border: selected === 2 ? "1px solid #F0C956" : "1px white solid"
                            }} className={"genderCard"} onClick={(e) => { setSelected(2) }}>
                                <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        height="140"
                                        image={homme}
                                        alt="green iguana"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                            Homme
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                        <Grid item lg={3}>
                            <Card sx={{ maxWidth: 345 }} style={{
                                border: selected === 3 ? "1px solid #F0C956" : "1px white solid"
                            }} className={"genderCard"} onClick={(e) => { setSelected(3) }}>
                                <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        height="140"
                                        image={ng}
                                        alt="green iguana"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                            Non-genre
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                    </Grid>
                    <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>

                    <Grid container justifyContent="flex-end">

                        <Button className={'buttonGender'} onClick={() => handleClick(30)}>Suivant</Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}