import { Typography, makeStyles, Box, Grid, Button, DialogActions } from "@material-ui/core";
import React, { useState } from "react"

import Alert from '@mui/material/Alert';
import { Link, useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import "../../style/profile.css";
import { useQuery } from "react-query";
import { format, parseISO } from 'date-fns'
import { fr } from 'date-fns/locale'
import config from "../../constant"
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';

import Logo from '../image/logo.png'


const useStyles = makeStyles(theme => ({
    menuList: {
        fontFamily: "satoshi",
        color: "white!important",
        textDecoration: "none!important",

        marginBottom: "14%",
        float: "right",
        "&  a": {
            color: "white",
        }
    },
    container: {
        marginBottom: "25px",
        borderBottom: "2px #F0C956 solid"
    },
    order: {
        backgroundColor: "#F0C956",
        fontWeight: 600,
        borderRadius: "25px",
        "&:hover": {
            border: "1px solid #F0C956",
            color: "white"
        }
    }

}));
const Profile = () => {

    const navigate = useNavigate()
    const classes = useStyles()
    const cookies = new Cookies();
    const token = cookies.get("TOKEN");

    const [order, setOrder] = useState(null)

    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = useState(null);

    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
    };

    const { data: orders, isLoading } = useQuery(['allOrdersByUsers', {
    }, `${config.url.API_URL}/getById`], () => fetch(`${config.url.API_URL}/order/getById`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            token: token
        }),
    }).then((res) => res.json()))
    if (!orders || isLoading) {
        return <p>Loading...</p>
    }
    return (
        <>
            <Grid container direction="row" style={{ height: "100%" }}>
                <Grid item className="leftNav" >
                    <Box className="boxNavShpz">
                        <Box className={"boxNavLogo"}>
                            <a href="/"> <img alt="avatar" className={"avatar"} src={Logo}></img></a>x
                        </Box>
                    </Box>

                    <Box className={"boxNav"}>
                        <img alt="avatar" className={"avatar"} src={Logo}></img>
                    </Box>
                    <Box className={"myOrder"}>
                        <Typography variant="h6">Mes commandes</Typography>
                    </Box>
                    <Box className={"myForm"}>
                        <Typography variant="h6">Mon questionnaire</Typography>
                    </Box>
                </Grid>


                <Grid item className={"tableItem"}>
                    <Box item className={classes.menuList}>
                        <Link className="menuLink" href={'/profile'}>Mon compte</Link>
                        <Link className="menuLink" href={"/"}>Nos offres</Link>
                        <Link className="menuLink">À propos</Link>
                        <Link className="menuLink">Contact</Link>

                        <Button className={classes.order} onClick={() => {
                            navigate('/agenda')
                        }}>Reserver une séance</Button>
                    </Box>
                    <TableContainer component={Paper}>

                        <Table sx={{ minWidth: 650 }} aria-label="simple table" style={{ backgroundColor: "#0f0f0f" }}>
                            <TableHead style={{ color: "white" }}>
                                <TableRow>
                                    <TableCell>Commande ID</TableCell>
                                    <TableCell align="right">Date</TableCell>
                                    <TableCell align="right">Détails</TableCell>
                                    <TableCell align="right">Montant</TableCell>
                                    <TableCell align="right">Validation</TableCell>
                                    <TableCell align="right">Type de commande</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {orders && orders.map((order) => (
                                    <TableRow
                                        key={order.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell scope="row">
                                            {`#${order.id || order._id}`}
                                        </TableCell>
                                        <TableCell align="right">{new Date(order.creationDate).toDateString()}</TableCell>
                                        <TableCell align="right" ><Button style={{ color: "#F0C956" }} onClick={() => { setSelectedValue(order.userId); setOpen(true); setOrder(order) }}>Apercu</Button> </TableCell>
                                        <TableCell align="right">{order.price}</TableCell>
                                        <TableCell align="right">{
                                            <>
                                                {order.state}
                                            </>
                                        }
                                        </TableCell>
                                        <TableCell align="right">{order.type}</TableCell>

                                    </TableRow>
                                ))}

                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>

            </Grid>


            <Grid container>

            </Grid>

            <DetailsDialog onClose={handleClose} open={open} selectedValue={selectedValue && selectedValue} order={order}></DetailsDialog>



        </>

    )

}


function DetailsDialog({ onClose, open, selectedValue, order }) {

    console.log(config)
    const { data, isLoadingUser } = useQuery(open && ['user', `${config.url.API_URL}/users/getOne`], () => fetch(`${config.url.API_URL}/users/getOne`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            userId: selectedValue
        }),

    }).then((res) => res.json()))
    const handleClose = () => {
        onClose(selectedValue);
    };
    if (isLoadingUser) {
        return <p>Loading...</p>
    }

    const classes = useStyles()
    return (
        <Dialog onClose={handleClose} open={open} className={"detailModal"} maxWidth={"md"}>
            <Box className="orderBox">
                <Grid container style={{ textAlign: "center" }} justifyContent="center">
                    <DialogTitle>
                        <Typography variant="h5" style={{ marginBottom: "15px", color: "white" }}>Détails de votre commande</Typography>
                    </DialogTitle>
                    {order && data && !order.schedule &&
                        <>
                            <Typography style={{ marginBottom: "15px", width: "100%" }}>Voici les informations transmit</Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre genre : <span style={{ color: "#F0C956" }}>{data.profileInfo.gender} </span></Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre silouette : <span style={{ color: "#F0C956" }}>{data.profileInfo.shape}</span> </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Vos mensuration : tete <span style={{ color: "#F0C956" }}>{data.profileInfo.size.cap}</span> , haut <span style={{ color: "#F0C956" }}>{data.profileInfo.size.shirt}</span>, bas  <span style={{ color: "#F0C956" }}>{data.profileInfo.size.pant}</span>  </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre besoin : <span style={{ color: "#F0C956" }}>{data.profileInfo.need} </span></Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Le prix choisit : <span style={{ color: "#F0C956" }}>{data.profileInfo.price} </span> </Typography>
                            {data.description &&
                                <Typography style={{
                                    color: "white",
                                    textAlign: "justify",
                                    width: "100%"
                                }}> Votre description : <span style={{ color: "#F0C956" }}>{data.profileInfo.description}</span> </Typography>
                            }
                            {data.instagram &&
                                <Typography style={{ color: "white", width: "100%" }}> Votre instagram : <span style={{ color: "#F0C956" }}>{data.profileInfo.instagram} </span></Typography>
                            }
                            {data.instagram &&
                                <Typography style={{ color: "white", width: "100%" }}> Votre instagram : <span style={{ color: "#F0C956" }}>{data.profileInfo.instagram} </span></Typography>
                            }
                            {order.state === "pending" ?
                                <Alert severity="warning" style={{ marginTop: "5%" }}> <Typography style={{ color: "black", width: "100%" }}> Votre commande n'a pas été regler, n'oubliez pas de regler votre comande au plus vite !</Typography>
                                </Alert>
                                : <Alert severity="success" style={{ marginTop: "5%" }}><Typography style={{ color: "white", width: "100%" }}>Votre commande a été validé nous nous occupons de vous</Typography></Alert>
                            }
                        </>
                    }
                    {data && order.schedule &&
                        <>
                            <Typography style={{ color: "white", width: "100%" }}> RDV le</Typography>
                            <Typography variant="h5" style={{ color: "white", width: "100%", marginBottom: "25px" }}> <span style={{ color: "#F0C956" }}>
                                {format(
                                    parseISO(order.scheduleInfo.meetingDate),
                                    "PPPpp",
                                    { locale: fr } // Pass the locale as an option
                                )}</span> </Typography>
                            <Typography style={{ color: "white", width: "100%" }}>  <span style={{ color: "#F0C956" }}>{order.scheduleInfo.name}</span></Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Numero lié : <span style={{ color: "#F0C956" }}> {order.scheduleInfo.phone}</span> </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre email <span style={{ color: "#F0C956" }}>{order.scheduleInfo.email}</span></Typography>

                            {!order.zoomLink ?
                                <Box style={{ marginTop: "5%", border: "1px #F0C956 solid" }}> <Typography style={{ color: "white", width: "100%" }}>Une heure avant votre RDV le lien pour le zoom recu par mail sera aussi disponible ici !</Typography>
                                </Box>
                                : <Button ><Typography style={{ color: "white", width: "100%" }}>Votre commande a été validé nous nous occupons de vous</Typography></Button>
                            }
                            {order.state === "pending" ?
                                <Alert severity="warning" style={{ marginTop: "5%" }}> <Typography style={{ color: "black", width: "100%" }}> Votre commande n'a pas été regler, n'oubliez pas de regler votre comande avant le rdv !</Typography>
                                </Alert>
                                : <Alert severity="success" style={{ marginTop: "5%" }}><Typography style={{ color: "white", width: "100%" }}>Votre commande a été validé nous nous occupons de vous</Typography></Alert>
                            }
                            <DialogActions>
                                <Button className={classes.order}>Annulé le rdv</Button>
                                <Button className={classes.order}>Deplacé le RDV</Button>
                            </DialogActions>
                        </>
                    }
                </Grid>
            </Box>
        </Dialog >
    );
}

export default Profile