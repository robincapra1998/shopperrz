import { Typography, Box, Grid, Button } from "@material-ui/core";
import React, { useState, useEffect, useCallback } from "react"
import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";
import { useMutation } from "react-query";
import config from "../../constant"

const updateUser = async (data) => {
    if (!data) return null
    return fetch(`${config.url.API_URL}/users/updateUser`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            profileInfo: data.profileInfo,
            token: data.token
        })
    }).then(res => res.json())

};


const createOrder = async (data) => {
    if (!data) return null
    return fetch(`${config.url.API_URL}/order`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            type: data.type,
            price: data.price,
            token: data.token
        })
    }).then(res => res.json())

};


const Final = () => {
    const cookies = new Cookies();
    const [info] = useState(cookies.get('userInfo'))

    const [finish, setfinish] = useState(false)
    const navigate = useNavigate();
    const [message, setmessage] = useState("");
    const handleClick = useCallback(
        () => {
            if (message.length === 0) {
                mutationOrder.mutate({
                    type: "form",
                    price: info.price,
                    token: cookies.get("TOKEN")
                })
                mutationUser.mutate({
                    profileInfo: info,
                    token: cookies.get("TOKEN")
                });

            }

            setfinish(true)
        },
        [setfinish, finish],
    )



    const mutationUser = useMutation(updateUser, {

        onSuccess: (data) => {


            console.log(data)
            if (data.status === 500) {

            }
            else {

            }
        },

    })
    const mutationOrder = useMutation(createOrder, {
        onSuccess: (data) => {
            console.log(data)
            if (data.status === 500) {

            }
            else {

            }
        },

    })

    useEffect(() => {
        return () => {
            var errors = []
            if (info === undefined) {
                console.log(info)
                if (!info || !info.gender) {
                    errors.push({ message: "Vous n'avez pas renseigner votre genre ", url: "/gender" })
                }
                if (!info || !info.shape) {
                    errors.push({ message: "Vous n'avez pas renseigner silouette ", url: "/shape" })
                }
                if (!info || !info.size) {
                    errors.push({ message: "Vous n'avez pas renseigner vos mensuration ", url: "/size" })
                }
                if (!info || !info.style) {
                    errors.push({ message: "Vous n'avez pas renseigner votre style  ", url: "/style" })
                }
                if (!info || !info.need) {
                    errors.push({ message: "Vous n'avez pas renseigner vos attente  ", url: "/need" })
                }
                if (!info || !info.price) {
                    errors.push({ message: "Vous n'avez pas renseigner votre prix", url: "/price" })
                }
            }
            setmessage(errors)
            if (info && info.gender && info.shape && info.size && info.style && info.need && info.price) {
                //todo validate 
                cookies.set('userInfo', JSON.stringify(info), { path: '/' });
                navigate("/final")
            }

        }
    }, [])

    return (
        <>
            <Menu></Menu>
            <Box className="finalBox">
                <Grid container style={{ textAlign: "center", justifyContent: "center" }}>
                    <Typography variant="h5" style={{
                        color: "white",
                        margin: '0 auto',
                        marginBottom: "20px"
                    }}>Récapitulatif commande</Typography>

                    {finish ? <Typography style={{ color: "white", marginBottom: "25px", alignContent: "center", justifyContent: "center" }} >Merci pour votre commande !</Typography> : <Typography style={{ color: "white", marginBottom: "25px" }} >Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</Typography>}
                    {finish && <Typography style={{ color: "white", marginBottom: "25px" }} >Rendez vous sur votre profile pour voir l'avancement de votre commande !</Typography>}

                    {info && finish === false &&
                        <>
                            <Typography style={{ color: "white", width: "100%", marginBottom: "15px" }}> Vos informations </Typography>
                            <Typography style={{ color: "white", width: "100%", textAlign: "left" }}> Votre genre : {info.gender} </Typography>
                            <Typography style={{ color: "white", width: "100%", textAlign: "left" }}> Votre silouette : {info.shape} </Typography>
                            <Typography style={{ color: "white", width: "100%", textAlign: "left" }}> Vos mensuration : tete {info.size.cap} , haut {info.size.shirt}, bas  {info.size.pant}  </Typography>
                            <Typography style={{ color: "white", width: "100%", textAlign: "left" }}> Votre besoin : {info.need} </Typography>
                            <Typography style={{ color: "white", width: "100%", textAlign: "left" }}> Le prix choisit : {info.price} </Typography>
                            {info.description &&
                                <Typography style={{
                                    color: "white",
                                    textAlign: "justify",
                                    width: "100%"
                                }}> Votre description : {info.description} </Typography>
                            }
                            {info.instagram &&
                                <Typography style={{ color: "white", width: "100%", textAlign: "left" }}> Votre instagram : {info.instagram} </Typography>
                            }
                        </>
                    }
                    {message && message.map((e) => {

                        return (
                            <Box>
                                <Typography style={{ textAlign: "center", marginTop: "25px", color: "white" }}>{e.message}</Typography>
                                <Button fullWidth className="buttonErrors" onClick={() => navigate(e.url)}></Button>
                            </Box>
                        )
                    }
                    )}
                    <Button onClick={handleClick} className="buttonConfirm">Confirmer</Button>
                </Grid>
            </Box>
        </>
    )

}

export default Final