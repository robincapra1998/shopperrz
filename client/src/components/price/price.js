import { Typography, Card, CardActionArea, CardContent, Box, Grid, Button } from "@material-ui/core";
import React, { useState } from "react"

import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";


const Price = () => {
    const cookies = new Cookies();
    const [info] = useState(cookies.get('userInfo'))
    const [selected, setSelected] = useState(0);

    const navigate = useNavigate();
    const [message, setmessage] = useState("");
    const handleClick = () => {
        if (selected === 0) {
            setmessage("veuillez renseigner un prix")
        }
        else {
            info.price = selected
            cookies.set('userInfo', JSON.stringify(info), { path: '/' });
            navigate("/more")
        }
    }

    return (
        <>
            <Menu></Menu>
            <Box>
                <Typography className={"genderTitle"} style={{ marginBottom: "25px" }}>Nos différents prix</Typography>
            </Box>
            <Grid container justifyContent='center' spacing={3}>
                <Grid item lg={3}>
                    <Card sx={{ maxWidth: 345, height: 450 }} style={{
                        border: selected === 30 ? "1px solid #F0C956" : "1px white solid"
                    }} className={"priceCard"} onClick={(e) => { setSelected(30) }}>
                        <CardActionArea>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div" align="center">
                                    30€
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
                <Grid item lg={3}>
                    <Card sx={{ maxWidth: 345 }} style={{
                        border: selected === 50 ? "1px solid #F0C956" : "1px white solid"
                    }} className={"priceCard"} onClick={(e) => { setSelected(50) }}>
                        <CardActionArea>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div" align="center">
                                    50€
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
                <Grid item lg={3}>
                    <Card sx={{ maxWidth: 345 }} style={{
                        border: selected === 80 ? "1px solid #F0C956" : "1px white solid"
                    }} className={"priceCard"} onClick={(e) => { setSelected(80) }}>
                        <CardActionArea>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div" align="center">
                                    80€
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            </Grid>
            <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>
            <Grid container justifyContent="space-between">
                <Button className={'buttonPrev'} onClick={() => navigate("/need")}>Précedent</Button>
                <Button className={'buttonGender'} onClick={() => handleClick()}>Suivant</Button>
            </Grid>

        </>

    )

}

export default Price