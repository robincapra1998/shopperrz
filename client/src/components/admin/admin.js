import { Typography, Box, Grid, Button } from "@material-ui/core";
import React, { useState } from "react"
import Menu from "../Menu";
import "../../style/dashboard.css";
import { useQuery } from "react-query";
import config from '../../constant'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';


const Admin = () => {
    console.log(config)
    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = useState(null);

    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
    };

    const { data: orders, isLoading } = useQuery(['allOrders', {
    }, `${config.url.API_URL}/auth-endpoint`], () => fetch(`${config.url.API_URL}/order/getAll`).then((res) => res.json()))

    if (!orders || isLoading) {
        return <p>Loading...</p>
    }

    return (
        <>
            <Menu></Menu>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Commande ID</TableCell>
                            <TableCell align="right">Date</TableCell>
                            <TableCell align="right">Détails</TableCell>
                            <TableCell align="right">Montant</TableCell>
                            <TableCell align="right">Paiment</TableCell>
                            <TableCell align="right">Validation</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {orders.map((order) => (
                            <TableRow
                                key={order.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {"#" + order.id}
                                </TableCell>
                                <TableCell align="right">{order.creationDate}</TableCell>
                                <TableCell align="right" ><Button onClick={() => { setSelectedValue(order.userId); setOpen(true) }}>Apercu</Button> </TableCell>
                                <TableCell align="right">{order.price}</TableCell>
                                <TableCell align="right">{order.state}</TableCell>
                                <TableCell align="right">{
                                    <>
                                        <Button data-id={order.userId}>Annuler</Button>
                                        <Button data-id={order.userId}>Confirmé</Button>
                                    </>
                                }
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <DetailsDialog onClose={handleClose} open={open} selectedValue={selectedValue && selectedValue}></DetailsDialog>



        </>

    )

}


function DetailsDialog({ onClose, open, selectedValue }) {

    const { data, isLoadingUser } = useQuery(open && ['user', `${config.url.API_URL}/users/getOne`], () => fetch(`${config.url.API_URL}/users/getOne`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            userId: selectedValue
        }),

    }).then((res) => res.json()))
    const handleClose = () => {
        onClose(selectedValue);
    };
    if (!data || isLoadingUser) {
        return <p>Loading...</p>
    }
    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>{data.email}</DialogTitle>
            <Box className="orderBox">
                <Grid container style={{ textAlign: "center" }} justifyContent="center">
                    {data &&
                        <>
                            <Typography style={{ marginBottom: "15px" }}>Information sur le profile </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre genre : {data.profileInfo.gender} </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre silouette : {data.profileInfo.shape} </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Vos mensuration : tete {data.profileInfo.size.cap} , haut {data.profileInfo.size.shirt}, bas  {data.profileInfo.size.pant}  </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Votre besoin : {data.profileInfo.need} </Typography>
                            <Typography style={{ color: "white", width: "100%" }}> Le prix choisit : {data.profileInfo.price} </Typography>
                            {data.description &&
                                <Typography style={{
                                    color: "white",
                                    textAlign: "justify",
                                    width: "100%"
                                }}> Votre description : {data.profileInfo.description} </Typography>
                            }
                            {data.instagram &&
                                <Typography style={{ color: "white", width: "100%" }}> Votre instagram : {data.profileInfo.instagram} </Typography>
                            }
                        </>
                    }
                </Grid>
            </Box>
        </Dialog>
    );
}

export default Admin