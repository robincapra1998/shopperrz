import { makeStyles } from "@material-ui/core/styles";
import Menu from "./Menu";
import React from "react";
import {
    useNavigate,
} from "react-router-dom";
import {
    Card,
    Grid,
    CardMedia,
    CardContent,
    Typography,
    Button,
    Box,
    CardActions,
} from "@material-ui/core";

import "../style/dashboard.css";
import img from "./cardpng.png";
import img2 from "./image2.png";
import img3 from "./image6.png";
import flow from "./image/flow.png";
import text from "./image/Texte.png";
import group8 from "./image/Group8.png";
import group7 from "./image/Group7.png";
import Footer from "./footer";



const useStyles = makeStyles((theme) => ({
    template: {
        width: "50%",
        margin: "0 auto",
    },
    header: {
        marginBottom: "100px",
    },
    white: {
        color: "white",
    },
    card: {
        /*  width: "413px", */
        paddingTop: "30px",
        paddingLeft: "30px",
        paddingRight: "30px",
        borderColor: "blue",
        background: " rgba(24, 24, 28, 0.5)",
        boxShadow: "inset 0px 4px 30px rgba(255, 255, 255, 0.09)",
        backdrop: "blur(6px)",
        borderRadius: "30px",
    },
    cardOffer: {
        width: "413px",
        background: " rgba(24, 24, 28, 0.5)",
        boxShadow: "inset 0px 4px 30px rgba(255, 255, 255, 0.09)",
        backdrop: "blur(6px)",
        borderRadius: "15px",
    },
    titleShop: {
        fontStyle: "normal",
        fontWeight: "900",
        fontSize: "42px",
        lineHeight: "58px",
        /* width: "125%", */
    },

    subShop: {
        fontfamily: "Unna",
        fontStyle: "italic",
        fontWeight: " 400",
        fontSize: "25px",
        lineHeight: " 58px",
    },
    img3: {
        position: "relative",
        bottom: "287px",
        right: "50%",
    },
    img2: {
        left: "7%",
        bottom: "191px",
        zIndex: "1",
        position: "relative",
    },
    price: {
        marginTop: "25px",
        marginBottom: "50px",
    },
    props: {
        position: "relative",
        bottom: "146px",
    },
    flowDiv: {
        display: "flex",
        justifyContent: "center",
        marginTop: "30px",
        marginBottom: "100px"
    },
    flowDiv2: {
        display: "flex",
        justifyContent: "center",
        marginBottom: "150px",
    },
    flow: {
        width: "60%",
    },

    hr: {
        justifyContent: "center",
        width: "90%",
        height: "3px",
        background:
            "linear-gradient(90deg, rgba(0,0,0,1) 0%, rgba(255,255,255,1) 50%, rgba(0,0,0,1) 100%)",
        margin: "0 auto",
    },
    boxGrid: {
        width: "43%",
        float: "right",
        paddingTop: "4%",
        paddingRight: "80px",
    },
    box: {
        marginTop: "100px",
        marginBottom: "100px",
    },
    box2: {
        /* float: right; */
        alignItems: "center",
        display: "flex",
        width: "43%",
        paddingLeft: "80px",
    },
    boxOffer: {
        margin: "0 auto",
        width: "90%",
    },
    button: {
        marginBottom: "40px",
        width: "196px",
        background: "#F0C956",
        border: "1px solid #F0C956",
        borderRadius: "40px",
        "&:hover": {
            color: "white",
        },
        fontWeight: "bold",
    },
    action: {
        justifyContent: "center",
    },
    titleOffer: {
        marginBottom: "100px",
    },
    resa: {
        marginTop: "60px",
        width: "300px",
    },
    img4: {
        position: "absolute",
        left: "81%",
        top: "426px",
    },
    boxFirst: {
        width: "50%",
        /*   marginRight: "98px", */
        margin: "0 14% 0px auto",
    },
    titleHistory: {
        marginTop: "100px",
        marginBottom: "100px",
    }
}));
export default function HomePage() {
    const classes = useStyles();
    const navigate = useNavigate();

    const handleClick = (price) => {
        navigate("/gender")
    }
    return (
        <>
            <div className={classes.header}>
                <Menu></Menu>
            </div>{/* 
            <img alt="boysandgirl" src={imgGirl} className={classes.img4} /> */}
            <Grid container alignItems="center" alignContent="right" style={{ marginBottom: "140px" }} >
                <Grid item lg={6}/* style={{ paddingRight: "100px" }} */>
                    <Box className={classes.boxFirst} alignSelf="right">
                        <Card className={classes.card}>
                            <CardMedia component="img" src={img}></CardMedia>
                            <CardContent>
                                <Typography
                                    gutterBottom
                                    variant="h5"
                                    component="div"
                                    className={classes.white}
                                >
                                    Lizard
                                </Typography>
                                <Typography variant="body2" className={classes.white}>
                                    Lizards are a widespread group of squamate reptiles, with over
                                    6,000 species, ranging across all continents except Antarctica
                                </Typography>
                            </CardContent>
                        </Card>
                    </Box>


                </Grid>

                <Grid item classe="white" lg={6} >
                    <Typography className={`${classes.white} ${classes.titleShop}`}>
                        Le future de la
                    </Typography>
                    <Typography className={`${classes.white} ${classes.titleShop}`}>
                        mode est <span className={classes.subShop}>SHOPPERZZ</span>
                    </Typography>
                    <Typography className={classes.white} style={{ width: "60%" }}>
                        Discutes avec nos professionnels de la mode et recrée toi un style avec des vêtements d'occasion
                        en relation avec tes attentes
                    </Typography>
                    <Button className={`${classes.resa} ${classes.button}`} onClick={() => {
                        navigate('/agenda')
                    }}>
                        Reserve ta séance
                    </Button>
                </Grid>
            </Grid>
            <Typography
                className={classes.white}
                justifyContent="center"
                textAlign="center"
                align="center"
                variant={"h4"}
            >
                Un style sur mesure avec des valeurs !
            </Typography>


            <div className={classes.flowDiv}>
                <img alt="boysandgirl" src={flow} align="center" className={classes.flow} />
            </div>
            <Box className={classes.box} mt={3}>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    style={{ marginBottom: "80px" }}

                >
                    <Grid item lg={6}>
                        <div className={classes.boxGrid}>
                            <Typography className={classes.white}>
                                Gagne du temps en déléguant tes achats de mode à un
                                professionnel du style.
                            </Typography>
                            <Typography className={classes.white}>
                                Nos personnals shopper font le tour des fripes en y combinant
                                leur réseau pour te proposer les piéces de 2nd mains adaptés à
                                tes critéres.
                            </Typography>
                        </div>
                    </Grid>
                    <Grid item lg={6}>
                        <img alt="boysandgirl" src={group8} align="center" className={classes.flow} />
                    </Grid>
                </Grid>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignContent="center"
                >
                    <Grid
                        item
                        lg={6}
                        alignContent="right"
                        alignItems="right"
                        justifyContent="right"
                    >
                        <img alt="boysandgirl" src={group7} align="right" className={classes.flow} />
                    </Grid>
                    <Grid item lg={6} alignItems="center" alignContent="center" style={{ display: "flex", alignItems: "center" }}>
                        <div className={classes.box2}>
                            {" "}
                            <Typography className={classes.white}>
                                Bénéficie de conseils sur mesure pour agrandir ta garde-robe et donner une seconde vie à la mode.
                            </Typography>
                        </div>
                    </Grid>
                </Grid>
            </Box>
            <Typography
                className={`${classes.white} ${classes.titleOffer}`}
                justifyContent="center"
                textAlign="center"
                align="center"
                variant={"h4"}
                style={{ marginTop: "205px" }}
            >
                Nos deux différentes offres
            </Typography>
            <Box className={classes.boxOffer} justifyContent={"center"}>
                <Grid container justifyContent="center" spacing={10}>
                    <Grid item>
                        <Card className={classes.cardOffer} sx={{ maxWidth: 345 }}>
                            <CardMedia component="img" image={group8} />
                            <CardContent>
                                <div className={classes.price}>
                                    <Typography
                                        className={classes.white}
                                        gutterBottom
                                        variant="h6"
                                        component="h6"
                                        align="center"
                                    >
                                        À partir de 30€
                                    </Typography>
                                </div>

                                <Typography
                                    variant="body2"
                                    className={classes.white}
                                    align="center"
                                    color="text.secondary"
                                >
                                    Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's
                                    standard dumbook
                                </Typography>
                            </CardContent>
                            <CardActions className={classes.action}>
                                <Button className={classes.button} onClick={() => handleClick(30)}>Suivant</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item>
                        <Card className={classes.cardOffer}>
                            <CardMedia component="img" src={group8}></CardMedia>
                            <CardContent>
                                <div className={classes.price}>
                                    <Typography
                                        gutterBottom
                                        variant="h6"
                                        component="h6"
                                        align="center"
                                        className={classes.white}
                                    >
                                        À partir de 50€
                                    </Typography>
                                </div>

                                <Typography
                                    variant="body2"
                                    className={classes.white}
                                    align="center"
                                >
                                    Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's
                                    standard dumbook
                                </Typography>
                            </CardContent>
                            <CardActions className={classes.action}>
                                <Button className={classes.button} onClick={() => navigate("/agenda")}>Suivant</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Box>
            <Typography
                className={`${classes.white} ${classes.titleHistory}`}
                justifyContent="center"
                textAlign="center"
                align="center"
                variant={"h4"}
                style={{ marginTop: "175px" }}
            >
                Notre histoire
            </Typography>
            <Box className={classes.box} mt={3}>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignContent="center"
                    style={{ marginBottom: "80px" }}
                >
                    <Grid item lg={6}>
                        <div className={classes.boxGrid}>
                            <Typography className={classes.white}>
                                Née d’un projet au départ étudiant et d’une passion commune ... la mode, nous avions à coeur de créer un service fiable et sécurisé portant des valeurs environnementales fortes.
                            </Typography>
                        </div>
                    </Grid>
                    <Grid item lg={6}>
                        <img alt="boysandgirl" src={group7} align="center" className={classes.flow} />
                    </Grid>
                </Grid>
            </Box>
            <Footer></Footer>
        </>

    );
}
