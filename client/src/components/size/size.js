import { Typography, RadioGroup, FormControl, FormControlLabel, Box, Grid, Button, Input, Radio } from "@material-ui/core";
import React, { useState } from "react"

import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";

import sizeh1 from "../image/couvre.png"
import sizeh2 from "../image/haut.png"
import sizeh3 from "../image/bas.png"
import sizeg1 from "../image/couvreG.png"
import sizeg2 from "../image/hautG.png"
import sizeg3 from "../image/basG.png"



const Size = () => {
    const cookies = new Cookies();
    const navigate = useNavigate();
    const [message, setmessage] = useState("");
    const [info] = useState(cookies.get('userInfo'));
    const [cap, setCap] = useState(null);
    const [shirt, setShirt] = useState(null);
    const [pant, setPant] = useState(null);
    const [heigt, setHeigt] = useState(null);
    const [weight, setWeight] = useState(null);

    const handleChangesCap = (event) => {
        setCap(event.target.value);
    };
    const handleChangesShirt = (event) => {
        setShirt(event.target.value);
    };
    const handleChangesPant = (event) => {
        setPant(event.target.value);
    };

    const handleClick = () => {
        if (!cap || !shirt || !pant || !heigt || !weight) {
            setmessage("veuillez renseigner les informations de taille")
        }
        else {
            info.size = {

                cap: cap,
                shirt: shirt,
                pant: pant,
                heigt: heigt,
                weight: weight

            }
            cookies.set('userInfo', JSON.stringify(info), { path: '/' });
            navigate("/style")
        }


    }

    return (
        <>
            <Menu></Menu>
            <Box >
                <Typography className={"genderTitle"} style={{ marginBottom: "25px" }} >Choisissez votre taille</Typography>
            </Box>
            <Grid container direction="row" justifyContent="space-around" >
                <Grid item style={{
                    display: "flex",
                    alignSelf: "center",
                }}>
                    <Typography className="inputSizeLabel">Taille</Typography>
                    <Input type="number" endAdornment="CM" className='sizeInput' value={heigt} onChange={(e) => setHeigt(e.target.value)}></Input>
                </Grid>
                <Grid item style={{
                    display: "flex",
                    alignSelf: "center",
                }}>
                    <Typography className={"inputSizeLabel"}>Poids</Typography>
                    <Input type="number" endAdornment="KG" className='sizeInput' value={weight} onChange={(e) => setWeight(e.target.value)}></Input>
                </Grid>
            </Grid>
            <Grid container className="boxSize" justifyContent="center" spacing={8}>
                <Grid item>
                    <Box >
                        <img alt="gender" className={"sizeImg"} src={info.gender === 1 ? sizeh1 : sizeg1}></img>
                        <FormControl>
                            <RadioGroup
                                aria-labelledby="demo-radio-buttons-group-label"
                                defaultValue="female"
                                name="radio-buttons-group"
                                value={cap}
                                onChange={handleChangesCap}
                            >
                                <FormControlLabel value="XS" control={<Radio className="check" />} label="XS" />
                                <FormControlLabel value="S" control={<Radio className="check" />} label="S" />
                                <FormControlLabel value="M" control={<Radio className="check" />} label="M" />
                                <FormControlLabel value="XL" control={<Radio className="check" />} label="XL" />
                            </RadioGroup>
                        </FormControl>

                    </Box>
                    <Typography style={{ paddingLeft: "25px" }}>Couvre chef</Typography>
                </Grid>
                <Grid item>
                    <Box >
                        <img alt="gender" className={"sizeImg"} src={info.gender === 1 ? sizeh2 : sizeg2}></img>

                        <FormControl>
                            <RadioGroup
                                aria-labelledby="demo-radio-buttons-group-label"
                                defaultValue="female"
                                name="radio-buttons-group"
                                value={shirt}
                                onChange={handleChangesShirt}
                            >
                                <FormControlLabel value="XS" control={<Radio className="check" />} label="XS" />
                                <FormControlLabel value="S" control={<Radio className="check" />} label="S" />
                                <FormControlLabel value="M" control={<Radio className="check" />} label="M" />
                                <FormControlLabel value="XL" control={<Radio className="check" />} label="XL" />

                            </RadioGroup>
                        </FormControl>

                    </Box>
                    <Typography style={{ paddingLeft: "19px" }}>Haut du corps</Typography>
                </Grid>
                <Grid item>
                    <Box >
                        <img alt="gender" className={"sizeImg"} src={info.gender === 1 ? sizeh3 : sizeg3}></img>
                        <FormControl>
                            <RadioGroup
                                aria-labelledby="demo-radio-buttons-group-label"
                                defaultValue="female"
                                name="radio-buttons-group"
                                value={pant}
                                onChange={handleChangesPant}
                            >
                                <FormControlLabel value="XS" control={<Radio className="check" />} label="XS" />
                                <FormControlLabel value="S" control={<Radio className="check" />} label="S" />
                                <FormControlLabel value="M" control={<Radio className="check" />} label="M" />
                                <FormControlLabel value="XL" control={<Radio className="check" />} label="XL" />
                            </RadioGroup>
                        </FormControl>
                    </Box>
                    <Typography style={{ paddingLeft: "35px" }}>Pantalon</Typography>
                </Grid>
            </Grid>
            <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>
            <Grid container justifyContent="space-between">
                <Button className={'buttonPrev'} onClick={() => navigate("/shape")}>Précedent</Button>
                <Button className={'buttonGender'} onClick={() => handleClick()}>Suivant</Button>
            </Grid>

        </>

    )

}

export default Size