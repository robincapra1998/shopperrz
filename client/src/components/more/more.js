import { Typography, Grid, Button, TextField } from "@material-ui/core";
import React, { useState } from "react"

import { Link, useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";


const More = () => {
    const cookies = new Cookies();
    const [info] = useState(cookies.get('userInfo'))

    const [description, setDescription] = useState('');
    const [instagram, setInstagram] = useState('');

    const navigate = useNavigate();
    const handleClick = () => {
        if (info.gender && info.shape && info.size && info.style && info.need && info.price) {
            info.description = description;
            info.instagram = instagram;
            cookies.set('userInfo', JSON.stringify(info), { path: '/' });
            navigate("/final")
        }
        navigate("/final")
    }

    return (
        <>
            <Menu></Menu>
            <Grid container justifyContent="center" alignContent="center" direction="column" style={{
                width: "33%",
                margin: '0 auto'
            }}>
                <Grid item>
                    <Typography className={"moreTitle"} style={{ marginBottom: "25px" }}>Montrez quelques photos de vous</Typography>
                </Grid>
                <Grid item>
                    <Typography>Dites-en plus sur vous</Typography>
                    <TextField multiline
                        fullWidth
                        className="moreTextArea"
                        minRows={6}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        label="Description">

                    </TextField>
                </Grid>
                <Grid item>
                    <Typography>Importez quelques photos de vous</Typography>
                </Grid>
                <Grid item>
                    <Typography>Instagram</Typography>
                    <TextField
                        type={"text"}
                        fullWidth
                        style={{ padding: "10px" }}
                        className="moreTextArea"
                        placeholder="@"
                        value={instagram}
                        onChange={(e) => setInstagram(e.target.value)}
                    >

                    </TextField>
                </Grid>
                <Grid item>
                    <Link className="skip" to={'/final'}>Passer cette étape</Link>
                </Grid>
            </Grid>

            <Grid container justifyContent="space-between">
                <Button className={'buttonPrev'} onClick={() => handleClick()}>Précedent</Button>
                <Button className={'buttonGender'} onClick={() => handleClick()}>Suivant</Button>
            </Grid>

        </>

    )

}

export default More