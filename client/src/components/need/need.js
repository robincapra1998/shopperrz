import { Typography, Box, Grid, Button } from "@material-ui/core";
import React, { useState } from "react"

import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Menu from "../Menu";
import "../../style/dashboard.css";


const Need = () => {
    const cookies = new Cookies();
    const [info] = useState(cookies.get('userInfo'))
    const [selected, setSelected] = useState(0);

    const navigate = useNavigate();
    const [message, setmessage] = useState("");
    const handleClick = () => {
        if (selected === 0) {
            setmessage("veuillez renseigner votre silouette")
        }
        else {
            info.need = selected
            cookies.set('userInfo', JSON.stringify(info), { path: '/' });
            navigate("/price")
        }
    }

    return (
        <>
            <Menu></Menu>
            <Box>
                <Typography className={"genderTitle"} style={{ marginBottom: "25px" }} >Choisissez votre silouette</Typography>
            </Box>
            <Box className="needBox">
                <Button className={'buttonNeed'} style={{ backgroundColor: selected === 1 ? "#F0C956" : "white" }} onClick={(e) => setSelected(1)}>Vêtements intemporel</Button>
                <Button className={'buttonNeed'} style={{ backgroundColor: selected === 2 ? "#F0C956" : "white" }} onClick={(e) => setSelected(2)}>Basiques indispensable</Button>
                <Button className={'buttonNeed'} style={{ backgroundColor: selected === 3 ? "#F0C956" : "white" }} onClick={(e) => setSelected(3)}>Vêtements actuels</Button>
                <Button className={'buttonNeed'} style={{ backgroundColor: selected === 4 ? "#F0C956" : "white" }} onClick={(e) => setSelected(4)}>Découvrir des marques tendance</Button>
                <Button className={'buttonNeed'} style={{ backgroundColor: selected === 5 ? "#F0C956" : "white" }} onClick={(e) => setSelected(5)}>Recherche de nouveauté</Button>
            </Box>
            <Typography style={{ textAlign: "center", marginTop: "25px" }} >{message}</Typography>
            <Grid container justifyContent="space-between">
                <Button className={'buttonPrev'} onClick={() => navigate("/style")}>Précedent</Button>
                <Button className={'buttonGender'} onClick={() => handleClick()}>Suivant</Button>
            </Grid>

        </>

    )

}

export default Need