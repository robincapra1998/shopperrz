import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import Login from './components/Login'
import HomePage from "./components/HomePage"
import Cookies from 'universal-cookie';
import "./style/dashboard.css";
import Gender from './components/user-form/gender';
import Shape from './components/shape/shape';
import Size from './components/size/size';
import Style from './components/style/style';
import Need from './components/need/need';
import Price from './components/price/price';
import More from './components/more/more';
import Agenda from './components/agenda/agenda';
import Final from './components/final/final';
import Register from './components/register/register';
import Admin from './components/admin/admin';
import { useQuery } from 'react-query';
import Profile from './components/profile/profile';
import config from './constant'

function App() {
  const cookies = new Cookies();

  const token = cookies.get("TOKEN");
  const { data: authent, isLoading } = useQuery(['auth', {
  }, `${config.url.API_URL}/auth-endpoint`], () => fetch(`${config.url.API_URL}/auth-endpoint`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json()))

  if (isLoading) {
    return <p>Loading...</p>
  }

  return (
    <>
      <Router>
        <Routes>
          <>
            <Route path="/" element={<HomePage></HomePage>}></Route>
            <Route path="/gender" element={token ? <Gender></Gender> : <Navigate to='/connexion' />}></Route>
            <Route path="/shape" element={token ? <Shape></Shape> : <Navigate to='/connexion' />}></Route>
            <Route path="/size" element={token ? <Size></Size> : <Navigate to='/connexion' />}></Route >
            <Route path="/style" element={token ? <Style></Style> : <Navigate to='/connexion' />}></Route >
            <Route path="/need" element={token ? <Need></Need> : <Navigate to='/connexion' />}></Route >
            <Route path="/price" element={token ? <Price></Price> : <Navigate to='/connexion' />}></Route >
            <Route path="/more" element={token ? <More></More> : <Navigate to='/connexion' />}></Route >
            <Route path="/final" element={token ? <Final></Final> : <Navigate to='/connexion' />}></Route >
            <Route path="/agenda" element={token ? <Agenda></Agenda> : <Navigate to='/connexion' />}></Route >
            <Route path="/admin" element={authent.isAdmin ? <Admin></Admin> : <Navigate to='/connexion' />}></Route >
            <Route path="/profile" element={authent.isAdmin ? <Profile></Profile> : <Navigate to='/connexion' />}></Route >
            <Route path="/register" element={<Register></Register>}></Route>
            <Route path="/connexion" element={<Login></Login>}></Route>
          </>
        </Routes >

      </Router ></>
  );
}


export default App;
