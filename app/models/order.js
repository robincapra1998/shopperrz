const mongoose = require("mongoose");


const Schema = new mongoose.Schema(
    {
        userId: {
            type: String
        },
        type: {
            type: String
        },
        creationDate: {
            type: Date,
            default: Date.now
        },
        state: {
            type: String
        },
        price: {
            type: Number
        },
        schedule: {
            type: String
        },
        scheduleInfo: {
            type: Object
        }
    },
    {
        collection: "order",
        minimize: false,
        versionKey: false,
        autoIndex: true
    }).set('toJSON', {
        transform: (doc, ret) => {
            ret.id = ret._id;
            delete ret._id;
        }
    });

module.exports = Schema;
