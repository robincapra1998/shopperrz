const mongoose = require("mongoose");
const profileInfoSchema = new mongoose.Schema({})

const SizeSchema = new mongoose.Schema({
  cap: {
    type: String
  },
  shirt: {
    type: String
  },
  pant: {
    type: String
  },
  heigt: {
    type: Number
  },
  weight: {
    type: Number
  },
});

const Schema = new mongoose.Schema(
  {
    profileInfo: {
      gender: {
        type: Number
      },
      shape: {
        type: String
      },
      need: {
        type: String
      },
      price: {
        type: Number
      },
      style: {
        type: String
      },

      size: {
        type: SizeSchema
      },

    },
    isAdmin: {
      type: Boolean
    },
    email: {
      type: String,
      required: [true, "Renseigné un email"],
      unique: [true, "Email Exist"],
    },

    password: {
      type: String,
      required: [true, "Renseigné un mot de passe!"],
      unique: false,
    },

  },
  {
    collection: "users",
    minimize: false,
    versionKey: false,
    autoIndex: true
  }).set('toJSON', {
    transform: (doc, ret) => {
      ret.id = ret._id;
      delete ret._id;
    }
  });

module.exports = Schema;
