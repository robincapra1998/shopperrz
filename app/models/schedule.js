const mongoose = require("mongoose");


const Schema = new mongoose.Schema(
    {
        email: {
            type: String,
        },
        name: {
            type: String,
        },
        lastName: {
            type: String,
        },
        creationDate: {
            type: Date,
            default: Date.now
        },
        meetingDate: {
            type: Date,
        },
        cancelUrl: {
            type: String,
        },
        updateScheduleUrl: {
            type: String,
        },
        uri: {
            type: String
        },
        phone: {
            type: String
        }
    },
    {
        collection: "schedule",
        minimize: false,
        versionKey: false,
        autoIndex: true
    }).set('toJSON', {
        transform: (doc, ret) => {
            ret.id = ret._id;
            delete ret._id;
        }
    });

module.exports = Schema;
