const jwt = require("jsonwebtoken");
const UserModel = require('../models/user.js');

module.exports = class Auth {
    constructor(app, connect) {
        this.app = app;
        this.UserModel = connect.model('User', UserModel);
        this.run();
    }
    run() {
        this.app.get("/auth-endpoint", async (request, response, next) => {
            try {

                //   get the token from the authorization header
                const token = await request.headers.authorization.split(" ")[1];

                //check if the token matches the supposed origin
                const decodedToken = await jwt.verify(token, "RANDOM-TOKEN");

                // retrieve the user details of the logged in user
                const user = await decodedToken;

                // pass the user down to the endpoints here
                request.user = user;

                var userInDb = await this.UserModel.findOne({ _id: user.userId }).exec()

                // pass down functionality to the endpoint
                if (userInDb.isAdmin) {
                    response.json({ message: "You are authorized to access me", token: token, isAdmin: true });
                }
                else {
                    response.json({ message: "You are authorized to access me", token: token });
                }


            } catch (error) {
                response.status(401).json({
                    error: new Error("Invalid request!"),
                });
            }


        })
    }
};

