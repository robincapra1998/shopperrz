const OrderModel = require('../models/order.js');
const SchedulesModel = require("../models/schedule.js")
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = class Order {
    constructor(app, connect) {
        this.app = app;
        this.OrderModel = connect.model('Order', OrderModel);
        this.SchedulesModel = connect.model('Schedule', SchedulesModel);
        this.run();
    }

    run() {
        this.app.get("/", (req, res) => {

            res.json({ message: "Hello from server!" });
        });


        this.app.post('/order/', (req, res) => {
            const token = req.body.token;
            //check if the token matches the supposed origin
            const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
            const user = decodedToken;
            // create a new user instance and collect the data
            console.log(user)
            const order = new this.OrderModel({
                userId: user.userId,
                type: req.body.type,
                state: "pending",
                price: req.body.price
            });

            // save the new user
            order.save()
                // return success if the new user is added to the database successfully
                .then((result) => {
                    res.status(201).send({
                        message: "Order Created Successfully",
                        result,
                    });
                })
                // catch error if the new user wasn't added successfully to the database
                .catch((error) => {
                    res.status(500).send({
                        message: "Error creating user",
                        error,
                    });
                });
        })


        this.app.post('/order/deleteOrder', (req, res) => {
            try {
                this.OrderModel.deleteOne({ _id: req.body.id }).then((user) => {
                    res.status(200).json(user || {});
                }).catch((err) => {
                    console.log(err);
                    res.status(200).json({ err });
                });

            } catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        //todo
        this.app.post('/order/updateOrder', async (req, res) => {
            try {
                const token = await req.body.token;
                //check if the token matches the supposed origin
                const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
                const user = await decodedToken;
                this.user
                this.OrderModel.findOneAndUpdate({
                    _id: user.userId
                }, req.body, {
                    new: true,
                    runValidators: true
                })
                    .then(dbUserData => {
                        console.log(req.body)
                        if (!dbUserData) {
                            res.status(404).json({
                                message: 'No user found with this id.'
                            });
                            return;
                        }
                        res.json(dbUserData);
                    })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        this.app.get('/order/getOne', (req, res) => {
            try {
                this.OrderModel.findOne({
                    _id: req.body.id
                }).then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
        this.app.post('/order/getById', async (req, res) => {
            try {
                const token = req.body.token;
                //check if the token matches the supposed origin
                const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
                const user = decodedToken;
                let foundedOrder = await this.OrderModel.find({
                    userId: user.userId
                })
                if (!foundedOrder) {

                    res.status(404).json({
                        code: 404,
                        message: "pas de commande trouvé"
                    })
                }
                for (let index = 0; index < foundedOrder.length; index++) {
                    const element = foundedOrder[index];
                    if (element.type === "custom") {
                        const scheduleFound = await this.SchedulesModel.find({
                            _id: element.schedule
                        })
                        foundedOrder[index] = foundedOrder[index].toObject()

                        foundedOrder[index].scheduleInfo = scheduleFound[0]

                        console.log(foundedOrder[index])
                    }

                }

                res.json(foundedOrder);

            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
        this.app.get('/order/getAll', (req, res) => {
            try {
                this.OrderModel.find().then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });


    }

}