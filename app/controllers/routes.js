const Users = require('./users.js')
const Auth = require('./auth')
const Order = require('./order')
const Schedules = require('./schedule')


module.exports = {
    Users,
    Auth,
    Order,
    Schedules,
};
