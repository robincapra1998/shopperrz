const UserModel = require('../models/user.js');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = class Users {
    constructor(app, connect) {
        this.app = app;
        this.UserModel = connect.model('User', UserModel);
        this.run();
    }

    run() {
        this.app.get("/", (req, res) => {

            res.json({ message: "Hello from server!" });
        });


        this.app.post('/users/', (req, res) => {
            // hash the password
            bcrypt.hash(req.body.password, 10)
                .then((hashedPassword) => {
                    console.log(hashedPassword)
                    // create a new user instance and collect the data
                    const user = new this.UserModel({
                        email: req.body.email,
                        password: hashedPassword,
                        isAdmin: false
                    });

                    // save the new user
                    user
                        .save()
                        // return success if the new user is added to the database successfully
                        .then((result) => {
                            res.status(201).send({
                                message: "User Created Successfully",
                                result,
                            });
                        })
                        // catch error if the new user wasn't added successfully to the database
                        .catch((error) => {
                            res.status(500).send({
                                message: "Error creating user",
                                error,
                            });
                        });
                })
            // catch error if the password hash isn't successful
            /*   .catch((e) => {
                  res.status(500).send({
                      message: "Password was not hashed successfully",
                      e: e,
                  });
              }); */
        });

        this.app.post('/users/deleteUser', (req, res) => {
            try {
                this.UserModel.deleteOne({ _id: req.body.id }).then((user) => {
                    res.status(200).json(user || {});
                }).catch((err) => {
                    console.log(err);
                    res.status(200).json({ err });
                });

            } catch (err) {
                console.error(`[ERROR] post:users -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        this.app.post('/users/updateUser', async (req, res) => {
            try {
                const token = await req.body.token;
                //check if the token matches the supposed origin
                const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
                const user = await decodedToken;
                this.user
                this.UserModel.findOneAndUpdate({
                    _id: user.userId
                }, req.body, {
                    new: true,
                    runValidators: true
                })
                    .then(dbUserData => {
                        console.log(req.body)
                        if (!dbUserData) {
                            res.status(404).json({
                                message: 'No user found with this id.'
                            });
                            return;
                        }
                        res.json(dbUserData);
                    })
            }
            catch (err) {
                console.error(`[ERROR] post:users -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        this.app.post('/users/getOne', (req, res) => {
            console.log(req.body.userId)
            try {
                this.UserModel.findOne({
                    _id: req.body.userId
                }).then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:users -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
        this.app.get('/users/getAll', (req, res) => {
            try {
                console.log(UserModel)
                this.UserModel.find().then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:users -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
        this.app.post('/users/connect', (request, response) => {
            // check if email exists
            this.UserModel.findOne({ email: request.body.email })

                // if email exists
                .then((user) => {
                    // compare the password entered and the hashed password found
                    bcrypt
                        .compare(request.body.password, user.password)

                        // if the passwords match
                        .then((passwordCheck) => {

                            // check if password matches
                            if (!passwordCheck) {
                                return response.status(400).send({
                                    message: "Passwords does not match",
                                    error,
                                });
                            }

                            //   create JWT token
                            const token = jwt.sign(
                                {
                                    userId: user._id,
                                    userEmail: user.email,
                                },
                                "RANDOM-TOKEN",
                                { expiresIn: "24h" }
                            );

                            //   return success response
                            response.status(200).send({
                                message: "Login Successful",
                                email: user.email,
                                token,
                            });
                        })
                        // catch error if password does not match
                        .catch((error) => {
                            response.status(400).send({
                                message: "Passwords does not match",
                                error,
                            });
                        });
                })
                // catch error if email does not exist
                .catch((e) => {
                    response.status(404).send({
                        message: "Email not found",
                        e,
                    });
                });
        })

    }

}