const SchedulesModel = require('../models/schedule.js');
const OrderModel = require('../models/order.js');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const axios = require('axios');
module.exports = class Schedules {
    constructor(app, connect) {
        this.app = app;
        this.SchedulesModel = connect.model('Schedule', SchedulesModel);
        this.OrderModel = connect.model('Order', OrderModel);
        this.run();
    }

    run() {
        this.app.get("/", (req, res) => {

            res.json({ message: "Hello from server!" });
        });

        this.app.post('/schedule/', async (req, res) => {
            console.log(req)
            const URLInvi = req.body.invitee.uri;
            const URLEvent = req.body.event.uri;

            const headers = {
                'Authorization': 'Bearer eyJraWQiOiIxY2UxZTEzNjE3ZGNmNzY2YjNjZWJjY2Y4ZGM1YmFmYThhNjVlNjg0MDIzZjdjMzJiZTgzNDliMjM4MDEzNWI0IiwidHlwIjoiUEFUIiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiJodHRwczovL2F1dGguY2FsZW5kbHkuY29tIiwiaWF0IjoxNjc1MTA2Nzk0LCJqdGkiOiIwMzE1NDhlZC02NmNhLTRjZDYtOGFmYi1lM2FkZGNhOTFhMWIiLCJ1c2VyX3V1aWQiOiJBREhDTUhOSjZJTkdJMktZIn0.cq5Y5AlSPPmx-a_KOH_bjCz8MFn81vg0dyFioXKSwgAi5oaIjEFIAmzVktc8L9q0_guM1nPSCL82AoX4wY6foQ'
            };

            var axiosResponseInvi = null
            var axiosResponseEvent = null

            try {
                axiosResponseInvi = await axios.get(URLInvi, { headers });
                axiosResponseEvent = await axios.get(URLEvent, { headers });

            } catch (error) {
                console.error(error);
            }
            try {

                const token = req.body.token;
                //check if the token matches the supposed origin
                const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
                const user = decodedToken;
                // create a new schedule instance and collect the data
                const schedule = new this.SchedulesModel({
                    email: axiosResponseInvi.data.resource.email,
                    name: axiosResponseInvi.data.resource.name,
                    lastName: axiosResponseInvi.data.resource.last_name,
                    meetingDate: axiosResponseEvent.data.resource.end_time,
                    cancelUrl: axiosResponseInvi.data.resource.cancel_url,
                    updateScheduleUrl: axiosResponseInvi.data.resource.reschedule_url,
                    uri: axiosResponseInvi.data.resource.uri,
                    phone: axiosResponseEvent.data.resource.location.location
                });



                // save the new schedule
                const savedSchedule = await schedule.save();


                const order = new this.OrderModel({
                    userId: user.userId,
                    type: "custom",
                    state: "pending",
                    price: 50,
                    schedule: savedSchedule.id,
                });
                // save the new order
                const savedOrder = await order.save();

                res.status(201).send({
                    message: "Order Created Successfully",
                    result: savedOrder,
                });
            } catch (error) {
                res.status(500).send({
                    message: "Error creating order",
                    error,
                });
            }
        });

        this.app.post('/schedule/schedule', (req, res) => {
            try {
                this.SchedulesModel.deleteOne({ _id: req.body.id }).then((user) => {
                    res.status(200).json(user || {});
                }).catch((err) => {
                    console.log(err);
                    res.status(200).json({ err });
                });

            } catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        //todo
        this.app.post('/schedule/updateSchedule', async (req, res) => {
            try {
                const token = await req.body.token;
                //check if the token matches the supposed origin
                const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
                const user = await decodedToken;
                this.user
                this.SchedulesModel.findOneAndUpdate({
                    _id: user.userId
                }, req.body, {
                    new: true,
                    runValidators: true
                })
                    .then(dbUserData => {
                        console.log(req.body)
                        if (!dbUserData) {
                            res.status(404).json({
                                message: 'No user found with this id.'
                            });
                            return;
                        }
                        res.json(dbUserData);
                    })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        this.app.get('/schedule/getOne', (req, res) => {
            try {
                this.SchedulesModel.findOne({
                    _id: req.body.id
                }).then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
        this.app.post('/schedule/getById', (req, res) => {
            try {
                const token = req.body.token;
                //check if the token matches the supposed origin
                const decodedToken = jwt.verify(token, "RANDOM-TOKEN");
                const user = decodedToken;
                this.SchedulesModel.find({
                    userId: user.userId
                }).then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
        this.app.get('/schedule/getAll', (req, res) => {
            try {
                this.SchedulesModel.find().then(dbUserData => {
                    if (!dbUserData) {
                        res.status(404).json({
                            message: 'No user found with this id.'
                        });
                        return;
                    }
                    res.json(dbUserData);
                })
            }
            catch (err) {
                console.error(`[ERROR] post:order -> ${err}`);

                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });
    }
}